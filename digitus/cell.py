"""The cell class.

The Cell class is a container for possible values in a given sudoku board cell. 
Cells may contain a single value when solved, or multiple values when the 
solution is not clear. Cells are iterable, and have some set-like operations
(union, difference, intersection, issuperset, issubset, and 
symmetric_diffrerence) which may be performed against other sets, Cells, and 
some more general container types. Cell values are altered using their assign()
method, which accepts either an integer, or a container of integers (including
another Cell).
"""

class Cell:
    """A single sudoku board cell."""
    def __init__(self, index, value):
        """Initialize the cell with its value.

        value - an integer in range(1,10), or a false value (None, '',
        False, 0, etc.) for unsolved cells.
        index - the board index of this cell, in the range [0,81].
                Indices start at 0 in the upper left corner, and
                proceed left to right, top to bottom with index 81 in the
                lower right corner.
        """
        self._index = index
        if value:
            if isinstance(value, int):
                if not 0 < int(value) < 10:
                    raise ValueError("Cell value ({}) must be in the range "
                                     "[1,9]".format(value))
                self.value = list((int(value),))
            else:
                self.value = list(value)
        else:
            self.value = list(range(1,10))

    @property
    def index(self):
        return self._index
        
    def union(self, other):
        """Behave as a set, and return the union of self and other"""
        return set(self.value).union(other)
    __or__ = union
    
    def difference(self, other):
        """Behave as a set and return the difference of self and other"""
        return set(self.value).difference(other)
    __sub__ = difference
    
    def intersection(self, other):
        """Behave as a set and return the intersection of self and other"""
        return set(self.value).intersection(other)
    __and__ = intersection
    
    def issubset(self, other):
        """Behave as a set and return True if self is a subset of other"""
        return set(self.value).issubset(other)
    __le__ = issubset
    
    def issuperset(self, other):
        """Behave as a set and return True if self is a superset of other"""
        return set(self.value).issuperset(other)
    __ge__ = issuperset
    
    def symmetric_difference(self, other):
        """Behave as a set and return the symmetric_difference of self and 
        other"""
        return set(self.value).symmetric_difference(other)
    __xor__ = symmetric_difference

    def __eq__(self, other):
        """Equality testing. 

        If other is a Cell, return True if Cells have the same values.
        If other is container, return set equality.
        If other is integer, return True iff len(Cell)==1 and cell 
        value == integer.
        Else raise ValueError.
        """
        if isinstance(other, int):
            if len(self) == 1 and self[0] == other:
                return True
            else:
                return False
        elif isinstance(other, (list, tuple, set, frozenset)):
            if len(self) != len(other):
                return False
            for v in self:
                if not v in other:
                    return False
            return True
        elif isinstance(other, Cell):
            return self.value == other.value
        else:
            raise ValueError("Can't compare Cell and {}".format(type(other)))

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self.value[item]
        else:
            raise TypeError('Cell indices must be integers or slices')

    def __iter__(self):
        return (v for v in self.value)

    def __len__(self):
        return len(self.value)

    def __hash__(self):
        """Our hash is our index, which should be unique and static"""
        return self.index
    
    def assign(self, other):
        """Assign a new value to this cell.

        other - One of: Integer, list, set, frozenset, or Cell
        return - difference in length of cell before and after assignment
        """
        olen = len(self)
        if isinstance(other, int):
            self.value = list((other,))
        elif isinstance(other, Cell):
            self.value = other.value
        else:
            self.value = list(other)
        return olen-len(self)

    @property
    def solved(self):
        """Return True if this cell is solved, False otherwise."""
        return len(self)==1

    @property
    def solution(self):
        """If cell is solved, return the value. Otherwise return None."""
        if not self.solved:
            return None
        return self.value[0]
    
    def __repr__(self):
        return "Cell({}, {!r})".format(self.index, self.value)

    def __str__(self):
        if not self.solved:
            return "?"
        else:
            return "{}".format(self.solution)
    
        
