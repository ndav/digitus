"""Example solver object"""

class SimpleSolver:
    """A simple sudoku solver implementation

    Should solve most easy to medium boards, and some more difficult ones.
    Uses two techniques to resolve unsolved cells.
    """

    def __init__(self, board):
        """ Initialize the solver.

        board - populated digitus sudoku Board to solve
        """
        self.board = board

    def solve(self):
        count = 1
        while count:
            count = 0
            count += self.exclude()
            count += self.unique()
            print('Modified {} cells'.format(count))
        print(self.board)
        if self.board.solved:
            print('Board is SOLVED')
        else:
            print('Board is UNSOLVED')

    def exclude(self):
        """Remove values from unsolved cells which are already solved in its 
        parent units"""
        count = 0
        for units in [self.board.rows, self.board.columns, self.board.blocks]:
            for u in units:
                solved = u.solved_values
                for c in u.unsolved_cells:
                    count += min(1, c.assign(c-solved))
        return count

    def unique(self):
        """Solve cells which have a unique value among all cells in its parent
        units"""
        count = 0
        for cell in self.board.cells:
            if len(cell) == 1:
                continue
            # Cells can also be pased to board.rows/columns/blocks to return
            # the parent row/cell/block for that cell.
            # Subtracting (difference operation) 'cell' from the returned
            # row/column/block returns a unit containing all cells from
            # that row/column/block except 'cell'
            row = self.board.rows[cell] - cell
            column = self.board.columns[cell] - cell
            block = self.board.blocks[cell] - cell
            if len(cell-row.values) == 1:
                # This cell contains a value unique to its entire row.
                # Therefor that must be this cell's value.
                cell.assign(cell-row.values)
                count += 1
            elif len(cell-column.values) == 1:
                cell.assign(cell-column.values)
                count += 1
            elif len(cell-block.values) == 1:
                cell.assign(cell-block.values)
                count += 1
        return count
