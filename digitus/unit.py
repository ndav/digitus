"""Classes for manipulating collections of Cells

Unit:
   A collection of Cell objects. Units provide convenience methods for 
   manipulating groups of Cells, including set-like operations (union,
   difference, intersection, issubset, issuperset, symmetric_difference), and
   methods that provide information about the Cells it contains. Units are 
   produced by a unit factory (UnitFactory), or created spontaneously as the 
   result of a Unit set operation.

UnitFactory:
   The UnitFactory is used by the Board class to generate Units in commonly
   desired configurations - rows, columns, sub-rows, sub-columns and blocks.
   A given UnitFactory has a type (one of the UnitType enumeration), and a 
   reference to the parent Board object. Units of the same type as the
   UnitFactory may be accessed by index, slice, or through iteration. In
   addition to indices and slices, UnitFactory's __getitem__ also accepts 
   Cells and Units, and will attempt to return a Unit of its UnitType which
   has a relation to the Cell or Unit. E.g. given a UnitFactory 'x' of type
   UnitType.ROW, and a Cell 'c', x[c] will return a Unit containing the row
   which contains Cell 'c'.

UnitType:
   A simple enumeration of common cell collections. UnitType.NONE and 
   UnitType.MISC are present for internal handling and completeness,
   and should not be used as a type for a UnitFactory.
"""

import enum
from digitus.cell import Cell

class UnitType(enum.Enum):
    """Useful sub-elements of a sudoku board"""
    NONE = 0
    MISC = 1
    SUBROW = 2
    SUBCOLUMN = 3
    ROW = 4
    COLUMN = 5
    BLOCK = 6

class UnitFactory:
    """Iterable factory for a given unit type."""
    def __init__(self, unit_type, board):
        """Initialize factory.

        unit_type - One of UnitType, ROW, COLUMN, SUBROW, SUBCOLUMN or BLOCK.  
        board - The parent board containing the cells we wish operate on.
        """
        if (unit_type in UnitType
            and unit_type is not UnitType.NONE
            and unit_type is not UnitType.MISC):
            self.unit_type = unit_type
        else:
            raise TypeError('unit_type must be one of ROW, COLUMN, SUBROW, '
                            'SUBCOLUMN, OR BLOCK')
        self.board = board
        
    def __getitem__(self, item):
        """Return the unit or units of this UnitType at this index or slice.

        Alternately, if item is a Unit, return the unit of this UnitType that
        the first cell in item is a member of.
        If item is a Cell, return the unit of this UnitType that item is a
        member of.
        """
        if isinstance(item, Cell):
            item = self.get_cell_unit_index(item)
        elif isinstance(item, Unit):
            item = self.get_cell_unit_index(item[0])
        if isinstance(item, slice):
            indexes = item.indices(len(self))
            return [x for i in range(*indexes) for x in self.get_unit(i)]  
        else:
            return self.get_unit(item)

    def __setitem__(self, item, value):
        raise TypeError('Units are read-only')
        
    def __iter__(self):
        """Iterate through all self.UnitTypes"""
        return (self[x] for x in range(len(self)))
        
    def __len__(self):
        if self.unit_type in [UnitType.ROW, UnitType.COLUMN, UnitType.BLOCK]:
            return 9
        elif self.unit_type in [UnitType.SUBROW, UnitType.SUBCOLUMN]:
            return 27

    def get_cell_unit_index(self, cell):
        """Return the unit index of this UnitType that 'cell' belongs to."""
        if self.unit_type == UnitType.SUBROW:
            return cell.index//3
        elif self.unit_type == UnitType.SUBCOLUMN:
            return ((cell.index//9)*9) + cell.index%9
        elif self.unit_type == UnitType.ROW:
            return cell.index//9
        elif self.unit_type == UnitType.COLUMN:
            return cell.index%9
        elif self.unit_type == UnitType.BLOCK:
            return ((cell.index//27)*3)+(cell.index//3)%3
        else:
            raise TypeError('UnitFactory of type {} cannot index cells'.format(
                self.unit_type))
        
    def get_unit(self, idx):
        """Return the Unit at idx for this UnitType."""
        name = self.unit_type.name.lower()
        return Unit(self.unit_type, getattr(self, 'get_'+name)(idx))
        
    def get_subrow(self, idx):
        """Return a sub-row unit at index idx."""
        assert 0 <= idx < 27
        origin = idx*3
        return self.board.cells[origin:origin+3]

    def get_subcolumn(self, idx):
        """Return a sub-column unit at index idx."""
        assert 0 <= idx < 27
        origin  = ((idx//9)*27)+((idx%9))
        return self.board.cells[origin:origin+27:9]

    def get_row(self, idx):
        """Return a row unit at index idx."""
        assert 0 <= idx < 9
        origin = idx*9
        return self.board.cells[origin:origin+9]

    def get_column(self, idx):
        """Return a column unit at index idx."""
        assert 0 <= idx < 9
        return self.board.cells[idx:idx+73:9]

    def get_block(self, idx):
        """Return a block unit at index idx."""
        assert 0 <= idx < 9
        origin = ((idx//3)*27)+((idx%3)*3)
        return (self.board.cells[origin:origin+3]
                + self.board.cells[origin+9:origin+12]
                + self.board.cells[origin+18:origin+21])
    
        
class Unit:
    """A collection of Cells with a UnitType"""
    all_solved = {1, 2, 3, 4, 5, 6, 7, 8, 9}
    
    def __init__(self, unit_type, cells):
        """Set the UnitType and populate cells"""
        self.unit_type = unit_type
        self.cells = cells

    def __getitem__(self, item):
        """Get a single Cell from this Unit.

        Returns the Cell at 'item' in this collection's order. Does not
        return the Cell with index 'item'.
        """
        return self.cells[item]

    def __iter__(self):
        return (c for c in self.cells)

    def __len__(self):
        return len(self.cells)

    @property
    def solved_values(self):
        """Return a set containing all the values in this unit that are
        already solved"""
        return set([c.solution for c in self if c.solved])

    @property
    def unsolved_values(self):
        """Return a set containing all the values in this unit that are not 
        solved for"""
        return set(range(1,10))-self.solved_values

    @property
    def values(self):
        """Return all values accounted for by this unit.

        For a full row, column, or block, this should always be the set
        {1, 2, 3, 4, 5, 6, 7, 8, 9}, as the values are either accounted for
        by solved cells, or listed as a possibility for unsolved cells.
        """
        return set.union(*[set(c.value) for c in self])
    
    @property
    def solved(self):
        """Return True if all cells in this unit are solved."""
        # If we're a block, column, or row, check that all contained
        # Cells are solved AND we account for all values (1-9).
        cells_solved = all([c.solved for c in self])
        if self.unit_type in [UnitType.ROW, UnitType.COLUMN, UnitType.BLOCK,
                              UnitType.SUBROW, UnitType.SUBCOLUMN]:
            return cells_solved and self.solved_values==self.all_solved
        else: # Otherwise, just check that all cells are solved
            return cells_solved

    @property
    def solved_cells(self):
        """Return a new Unit containing all solved cells in this unit."""
        return Unit(UnitType.MISC, [c for c in self if c.solved])
    
    @property
    def unsolved_cells(self):
        """Return a new Unit containing all usolved cells in this unit."""
        return Unit(UnitType.MISC, [c for c in self if not c.solved])

    def __eq__(self, other):
        """Compare two units. Units are equal if they are then same size,
        and contain cells with the same value, in the same order."""
        if len(self) != len(other):
            return False
        for c, o in zip(self, other):
            if c!= o:
                return False
        return True
    
    def union(self, other):
        """Behave as a set, and return the union of self and other."""
        if isinstance(other, Cell):
            other = set([other])
        return Unit(UnitType.MISC, list(set(self.cells).union(set(other))))
    __or__ = union
    
    def difference(self, other):
        """Behave as a set and return the difference of self and other."""
        if isinstance(other, Cell):
            other = set([other])
        return Unit(UnitType.MISC, list(set(self.cells).difference(
            set(other))))
    __sub__ = difference
    
    def intersection(self, other):
        """Behave as a set and return the intersection of self and other."""
        if isinstance(other, Cell):
            other = set([other])
        return Unit(UnitType.MISC, list(set(self.cells).intersection(
            set(other)))) 
    __and__ = intersection
    
    def issubset(self, other):
        """Behave as a set and return True is self is a subset of other."""
        if isinstance(other, Cell):
            other = set([other])
        return set(self.cells).issubset(set(other))
    __le__ = issubset
    
    def issuperset(self, other):
        """Behave as a set and return True is self is a superset of other."""
        if isinstance(other, Cell):
            other = set([other])
        return set(self.cells).issuperset(set(other))
    __ge__ = issuperset
    
    def symmetric_difference(self, other):
        """Behave as a set and return the symmetric_difference of self and 
        other."""
        if isinstance(other, Cell):
            other = set([other])
        return Unit(UnitType.MISC, list(set(self.cells).symmetric_difference(
            other)))
    __xor__ = symmetric_difference
    
    def __str__(self):
        """Return the string repr. of this unit.

        Some unit types (notably, UnitType.BLOCK) may return a string that
        contains newlines. UnitType.ROW units and UnitType.COLUMN units
        segregate cells in chunks of three separated by spaces.
        """
        vals = [str(c) for c in self.cells]
        if self.unit_type == UnitType.ROW or self.unit_type == UnitType.COLUMN:
            return '{}{}{} {}{}{} {}{}{}'.format(*vals)
        elif self.unit_type == UnitType.BLOCK:
            return '{}{}{}\n{}{}{}\n{}{}{}'.format(*vals)
        elif (self.unit_type == UnitType.SUBROW
              or self.unit_type == UnitType.SUBCOLUMN):
            return '{}{}{}'.format(*vals)
        else:
            return ''.join(vals)
