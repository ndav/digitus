"""Classes for handling the sudoku board."""
from digitus.cell import Cell
from digitus import unit

class Board:
    """A sudoku board, composed of 81 Cells"""
    def __init__(self, values):
        """Initialize sudoku board.
        
        values - an iterable of 81 integers or false values ('', 0, False, 
                 None, etc.) for non-populated slots. Values are read left
                 to right, top to bottom.
        """
        self.cells = tuple([Cell(i, v) for (i, v) in enumerate(values)])
        self.subrows = unit.UnitFactory(unit.UnitType.SUBROW, self)
        self.subcolumns = unit.UnitFactory(unit.UnitType.SUBCOLUMN, self)
        self.rows = unit.UnitFactory(unit.UnitType.ROW, self)
        self.columns = unit.UnitFactory(unit.UnitType.COLUMN, self)
        self.blocks = unit.UnitFactory(unit.UnitType.BLOCK, self)

    @property
    def solved(self):
        """Return True if board is solved, else False."""
        return all([b.solved for b in self.blocks])
        
    def __str__(self):
        ret = ''
        for i,r in enumerate(self.rows):
            ret += str(r)+'\n'
            if (i%3) == 2:
                ret += '\n'
        return ret[:-2]
        

