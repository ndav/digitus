import unittest
from digitus import unit
from digitus import Cell

class TestUnit(unittest.TestCase):
    """Test digitus Unit class functionality"""

    def setUp(self):
        """Set up a handful of units for testing"""
        self.urow = unit.Unit(unit.UnitType.ROW,
                               [Cell(v-1, v) for v in range(1,10)])
        self.ucolumn = unit.Unit(unit.UnitType.COLUMN,
                                 [Cell(i, v) for i, v in
                                  zip([0,9,18,27,36,45,54,63],
                                      [1,4,7,2,3,5,6,8,9])])
        self.ublock = unit.Unit(unit.UnitType.BLOCK,
                                [Cell(i, v) for i,v in zip([0,1,2,9,10,11,
                                                            18,19,20],
                                                           range(1,10))])
    def test_unit_difference(self):
        """Test difference operation on units"""
        self.assertEqual(self.ublock-self.urow, [4, 5, 6, 7, 8, 9])
        self.assertEqual(self.ublock-self.ucolumn, [2, 3, 5, 6, 8, 9])        
        self.assertEqual(self.urow-self.ucolumn, [2, 3, 4, 5, 6, 7, 8, 9])
