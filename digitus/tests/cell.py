import unittest
from digitus import Cell
import random

class CellTest(unittest.TestCase):
    unsolved = set([1,2,3,4,5,6,7,8,9])
                
    def test_cell_from_container(self):
        """Test Cells can be assigned from and compared to various containers"""
        for list_type in [[1,5,3,7], set([8,2,4,2]), tuple([5,2,6,7])]:
            c = Cell(0, list_type)
            self.assertEqual(c, list_type)
            
    def test_cell_false(self):
        """Test Cell assignment from false values"""
        for null in [None, '', 0, False, [], set(), dict(), frozenset()]:
            c = Cell(0, null)
            self.assertEqual(c, self.unsolved)

    def test_cell_operators(self):
        """Test that cell operators (&,|,<,>,^, -) are correctly assigned"""
        c = Cell(0,False)
        self.assertEqual(c.__or__, c.union)
        self.assertEqual(c.__sub__, c.difference)
        self.assertEqual(c.__and__, c.intersection)
        self.assertEqual(c.__le__, c.issubset)
        self.assertEqual(c.__ge__, c.issuperset)
        self.assertEqual(c.__xor__, c.symmetric_difference)
        
    def test_cell_set_operations(self):
        """Test Cell set operations.
    
        We're using random input data, but a known seed and large-ish sample
        size.
        """
        random.seed('rincewind')
        for i in range(1000):
            # Only test for non-empty sets, since empty cells don't exist
            cval = set(random.sample(range(1,10),random.randint(1,9)))
            oval = set(random.sample(range(1,10),random.randint(0,9)))
            cell = Cell(0, cval)
            msg = 'cell: {} ({}) other: {}'.format(cell.value, cval, oval)
            self.assertEqual(cell.issuperset(oval), cval.issuperset(oval), msg)
            self.assertEqual(cell.issubset(oval), cval.issubset(oval), msg)
            self.assertEqual(cell==oval, cval==oval, msg)
            self.assertEqual(cell.union(oval), cval.union(oval), msg)
            self.assertEqual(cell.difference(oval), cval.difference(oval), msg)
            self.assertEqual(cell.intersection(oval), cval.intersection(oval),
                             msg)
            self.assertEqual(cell.symmetric_difference(oval),
                             cval.symmetric_difference(oval), msg)
            
if __name__ == '__main__':
    unittest.main()        
