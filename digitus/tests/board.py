import unittest
from digitus import Board
from digitus import Unit, UnitType

testboard = ['','', 1,  '', 2,'',  '','','',
             '','','',   4, 7, 8,  '','', 1,
             '','',None,   3,'','',  '', 7, 8,
             
             '', 3,'',  '','', 7,  '', 5, 2,
             '', 2,'',   1, 9, 5,  '', 4,'',
              5, 8,'',   2,False,'',  '', 9,'',
             
              4, 6,'',  '','', 2,  '','','',
              7,'',[],   9, 1, 4,   0,'','',
             '','','',  '', 8,'',   7,'','']


class BoardTest(unittest.TestCase):
    """ Test the Board class"""
    u = set(range(1,10)) # An unsolved cell
    
    def setUp(self):
        """Initialize ourselves a board to play with"""
        self.test_board_populate()

    def test_board_populate(self):
        """Test that Board populates correctly"""
        self.board = Board(testboard)
        for b, c in zip(testboard, self.board.cells):
            if not b:
                b = set(range(1,10))
            self.assertEqual(b, c)
            
    def test_board_row_factory(self):
        """Test row factory for correct rows, including slicing"""
        r0 = Unit(UnitType.ROW, (self.u, self.u, 1, self.u, 2, self.u,
                                 self.u, self.u, self.u))
        r3 = [self.u, 3, self.u, self.u, self.u, 7, self.u, 5, 2]
        r5 = [5, 8, self.u, 2, self.u, self.u, self.u, 9, self.u]
        self.assertEqual(self.board.rows[0], r0)
        self.assertEqual(self.board.rows[3], r3)
        self.assertEqual(self.board.rows[5], r5)
        # Test slice with stride
        self.assertEqual(self.board.rows[3:6:2], r3+r5)
        
    def test_board_column_factory(self):
        """Test column factory for correct columns, including slicing"""
        c0 = Unit(UnitType.COLUMN, [self.u, self.u, self.u, self.u, self.u,
                                    5, 4, 7, self.u])
        c3 = [self.u, 4, 3, self.u, 1, 2, self.u, 9, self.u]
        c5 = [self.u, 8, self.u, 7, 5, self.u, 2, 4, self.u]
        self.assertEqual(self.board.columns[0], c0)
        self.assertEqual(self.board.columns[3], c3)
        self.assertEqual(self.board.columns[5], c5)
        # Test slice with stride
        self.assertEqual(self.board.columns[3:6:2], c3+c5)

    def test_board_block_factory(self):
        """Test block factory for correct blocks, including slicing"""
        b0 = Unit(UnitType.BLOCK, [self.u, self.u, 1, self.u, self.u, self.u,
                                   self.u, self.u, self.u])
        b3 = [self.u, 3, self.u, self.u, 2, self.u, 5, 8, self.u]
        b5 = [self.u, 5, 2, self.u, 4, self.u, self.u, 9, self.u]
        self.assertEqual(self.board.blocks[0], b0)
        self.assertEqual(self.board.blocks[3], b3)
        self.assertEqual(self.board.blocks[5], b5)
        # Test slice with stride
        self.assertEqual(self.board.blocks[3:6:2], b3+b5)

    def test_board_subrow_factory(self):
        """Test subrow factory for correct subrows, including slicing"""
        sr0 = Unit(UnitType.SUBROW, [self.u, self.u, 1])
        sr10 = [self.u, self.u, 7]
        sr20 = [self.u, self.u, self.u]
        self.assertEqual(self.board.subrows[0], sr0)
        self.assertEqual(self.board.subrows[10], sr10)
        self.assertEqual(self.board.subrows[20], sr20)
        # Test slice with stride
        self.assertEqual(self.board.subrows[0:21:10], sr0.cells+sr10+sr20)
        
    def test_board_subcolumn_factory(self):
        """Test subcolumn factory for correct subcolumns, including slicing""" 
        sc0 = Unit(UnitType.SUBCOLUMN, [self.u, self.u, self.u])
        sc12 = [self.u, 1, 2]
        sc24 = [self.u, self.u, 7]
        self.assertEqual(self.board.subcolumns[0], sc0)
        self.assertEqual(self.board.subcolumns[12], sc12)
        self.assertEqual(self.board.subcolumns[24], sc24)
        # Test slice with stride
        self.assertEqual(self.board.subcolumns[0:25:12], sc0.cells+sc12+sc24)
        
