from setuptools import setup

setup(name='digitus',
      version='0.1',
      description='Sudoku solver and tools',
      url='',
      download_url='',
      author='tty72',
      author_email='admin@tty72.com',
      license='GPLv3',
      packages=['digitus', 'digitus.tests', 'digitus.samples'],
      test_suite = 'digitus.tests',
      zip_safe=False)
