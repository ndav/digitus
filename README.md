# Digitus

Digitus is a Python 3 module for working with Sudoku boards. It provides classes to allow manipulation of the board through set-like operators. Digitus is pure Python 3, but shouldn't be difficult to convert to earlier versions of Python either by hand, or using a tool like [Pasteurize](http://python-future.org/pasteurize.html).


Sample usage:
```python
>>> from digitus import Board
>>> from digitus.solver import SimpleSolver
>>> from digitus.samples import boards
>>> b = Board(boards.medium)
>>> print(b)
??1 ?2? ???
??? 478 ??1
??? 3?? ?78

?3? ??7 ?52
?2? 195 ?4?
58? 2?? ?9?

46? ??2 ???
7?? 914 ???
??? ?8? 7??
>>> print(b.rows[0])
??1 ?2? ???
>>> print(b.blocks[0])
??1
???
???
>>> s = SimpleSolver(b)
>>> s.solve()
Modified 136 cells
Modified 38 cells
Modified 23 cells
Modified 0 cells
871 629 435
395 478 261
246 351 978

139 847 652
627 195 843
584 263 197

468 732 519
752 914 386
913 586 724
Board is SOLVED
```

Further information can be found by examing the docstrings, the included non-exhaustive test suite, or the basic solver included in the module. Digitus also has something that resembles a [homepage](http://tty72.com/sudoku).
